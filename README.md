# Shopping list #

This is a simple script that is used to demonstrate the usage of the browsers local storage.

After you checked out the project you will have to run

    bower update

If you don´t know how to use bower: [http://bower.io/](http://bower.io/)

### Live-Demo

[Shopping List](http://projects.rene-springmann.de/shopping-list/)