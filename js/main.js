$( document ).ready(function() {
    if(typeof(Storage) === "undefined") {
        $('#notCompatible').show();
        $('#list').hide();

        return;
    }

    //Initialize the list with some examples
    if(localStorage.getItem('shopping-list-items') === null) {
        localStorage.setObject('shopping-list-items', {
            'Apple': {
                'count': 10
            },
            'Milk': {
                'count': 3
            },
            'Noodles': {
                'count': 1
            }
        });
    }

    //Bin events
    $('#clearList').click(clearList);

    $('#newItemForm').submit(addItem);

    $('body').on('click', 'input.gotIt', gotIt);

    //Initially show the list
    refreshList();
});

/**
 * Refreshes the list view
 */
function refreshList()
{
    var list = $('#list ul');
    var counter = $('#numOfItems');
    var items = localStorage.getObject('shopping-list-items');

    list.html('');
    if(typeof(items) == "undefined" || items === null) { //nothing in the list
        counter.html( 0 );

        return;
    }

    counter.html( Object.keys(items).length );//refresh counter

    $.each(items, function(name, item){ //add all the items
        list.append(
            '<li data-index="' + name + '">' +
                '<div>' +
                    '<input class="gotIt" type="button" value="Got It">' +
                '</div>' +
                '<div class="itemCount">' +
                    item.count +
                '</div>' +
                '<div class="itemName">' +
                    name +
                '</div>' +

            '</li>'
        );
    });
}

/**
 * Deletes all items from list and clears the view
 */
function clearList()
{
    localStorage.setObject('shopping-list-items', {}); //delete stored items
    var list = $('#list ul');
    list.find('li').slideUp(400, refreshList); //smoothly slide the content up and reresh the view
}

/**
 * Handles the form submit and therefor adds a new item to the list
 *
 * @returns {boolean}
 */
function addItem()
{
    var itemNameInput = $('#itemName');
    var itemCountInput = $('#itemCount');
    var itemName = ucfirst($.trim(itemNameInput.val()));
    var itemCount = itemCountInput.val();
    var items = localStorage.getObject('shopping-list-items');
    var list = $('#list ul');
    var counter = $('#numOfItems');
    //console.debug(items);
    if(itemName == '') {
        //reset form
        itemCountInput.val(1);
        itemNameInput.val('').focus();

        return false;//prevent sending the form
    }

    if( itemName in items) { //item already in list
        items[itemName].count = items[itemName].count * 1 + itemCount * 1 ; // add counter to items counter
        localStorage.setObject('shopping-list-items', items);
        refreshList();
    }
    else { //item not in list, add a new one
        items[itemName] = { count: itemCount };
        localStorage.setObject('shopping-list-items', items);
        list.append(
            '<li data-index="' + itemName + '" style="display:none">' +
                '<div>' +
                    '<input type="button" class="gotIt" value="Got It">' +
                '</div>' +
                '<div class="itemCount">' +
                    itemCount +
                '</div>' +
                '<div class="itemName">' +
                    itemName +
                '</div>' +
            '</li>'
        );
        list.find('li:hidden').slideDown(); //smoothly slide in
        counter.html(Object.keys(items).length);//update counter
    }

    //reset form
    itemCountInput.val(1);
    itemNameInput.val('').focus();

    return false; //prevent sending the form
}

function gotIt()
{
    var button = $(this);
    var li = button.closest('li');
    var itemName = li.data('index');
    var items = localStorage.getObject('shopping-list-items');

    delete items[itemName];
    localStorage.setObject('shopping-list-items', items);
    li.slideUp(400, refreshList);

}

//expand the localStorage with handles for Objects
Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);

    return value && JSON.parse(value);
};

/**
 * Transforms the first char to an uppercase char
 *
 * @param str
 * @returns {string}
 */
function ucfirst(str) {
    //  discuss at: http://phpjs.org/functions/ucfirst/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    //   example 1: ucfirst('kevin van zonneveld');
    //   returns 1: 'Kevin van zonneveld'

    str += '';
    var f = str.charAt(0).toUpperCase();

    return f + str.substr(1);
}